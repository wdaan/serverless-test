import * as functions from 'firebase-functions';
import * as express from 'express'
const app = express()

export const helloWorld = functions.https.onRequest((request, response) => {
    response.send("Hello from Firebase!");
});


export const greet = functions.https.onRequest((req, res) => {
    const { name } = req.query;
    if (name) {
        res.send(`Hello ${name}!`)
        return
    }
    res.send('no name was specified')
})

export const add = functions.https.onRequest((req, res) => {
    const { a, b } = req.body;
    if (Number.isInteger(a) && Number.isInteger(b)) {
        res.send(`Sum is : ${a + b}`)
        return
    }
    res.send('No valid input specified')
})

export const api = functions.https.onRequest(app)

app.get("/", (req, res) => {
    res.send("Successfull api call!")
})

app.get('/data', (req, res) => {
    res.send('Successful data call! :smile:')
})